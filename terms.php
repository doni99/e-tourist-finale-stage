<?php
session_start();
error_reporting(0);
include('includes/config.php');
?>
<!DOCTYPE HTML>
<html>
<head>
<title>e-Tourist</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,700,600' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
<link href="css/font-awesome.css" rel="stylesheet">

<script src="js/jquery-1.12.0.min.js"></script>
<script src="js/bootstrap.min.js"></script>



</head>
<body>
<?php include('includes/headeri.php');?>



<div class="banner-3">
	<div class="container">
		<h1> Kushtet e perdorimit</h1>
	</div>
</div>




<div class="rooms">
	<div class="container">
		
		<div class="room-bottom">
			<h3>Mire se vini</h3>

			<h4> Ju faleminderit që keni zgjedhur të përdorni TMS(E-turist).com, TMS(E-turist)Video dhe shërbimet tona tjera, të cilave në tërësi do t’i referohemi si “Shërbimet”.

Ne kemi hartuar këto kushte të përdorimit (të cilave do t’i referohemi si “Kushtet”) ashtu që të njoftoheni me rregullat të cilat definojnë marrëdhënien tonë me ju. Edhe pse jemi munduar që kjo rregullore të jetë sa më e kuptueshme, në të do të hasni edhe terme ligjore. Arsyeja e përdorimit të tyre është se formojnë nenet dhe relacion të detyrueshëm kontraktues ndërmjet juve dhe TMS(E-turist), Inc. kështu që ju sugjerojmë që t’i lexoni me kujdes.

Duke shfrytëzuar shërbimet tona, ju pajtoheni dhe merrni përsipër të respektoni në tërësi obligimet tuaja që rrjedhin nga këto Kushte, në të kundërtën me mirësi ju lusim të mos i përdorni shërbimet tona.

Në kuadër të këtyre Kushteve, termet “JU” apo “përdorues” apo “blerës” nënkuptojnë entitetin apo individin i cili përdor shërbimet tona, ndërsa “NE” apo “tona” apo “TMS(E-turist)” nënkupton TMS(E-turist), Inc., dega në Kosovë, dhe “palët” nënkuptojnë JU dhe TMS(E-turist).

TMS(E-turist)Video përmban kanale qe i dedikohen audiencës së moshës minorene, me ç’rast kushtet e shërbimit duhet pranuar nga prindi ose kujdestari ligjor. Prindi/kujdestari ligjor zotohet që do t’ju përmbahet këtyre kushteve dhe në raport me fëmiun, do të kujdeset dhe mban përgjegjësi për shfaqjen e përmbajtjes. . </h4> 

		<h3>Te drejtat e autorit</h3>

		<h4>Ne jemi të përkushtuar të mbrojmë të drejtat e autorit dhe angazhohemi që këto të drejta të respektohen në tërësi.

E gjithë përmbajtja e përfshirë, e vënë në dispozicion apo e plasuar përmes ndonjë shërbimi tonë e cila në mënyrë shprehimore theksohet se është vënë në dispozicion ose plasuar nga TMS(E-turist), siç mund të jenë por pa u limituar: ndonjë tekst, grafike, llogo, imazh, audio klip, shkarkime digjitale, të dhëna të grumbulluara dhe/apo softuere janë pronë e jona, apo e kontraktorëve tanë dhe mbrohen me ligjin mbi të drejtat e autorit të Republikës se Kosovës, ligjet ku operon apo ushtron aktivitetin TMS(E-turist) dhe normat ndërkombëtare për të drejtat e autorit.

Nëse gjatë shfrytëzimit të shërbimeve tona vëreni se ndonjë e drejtë e autorit është cenuar ju lutem mos hezitoni të na kontaktoni përmes të dhënave të kontaktit të publikuara në ueb faqen tonë.

Në anën tjetër, kanalet të cilat ndodhen në TMS(E-turist)Video nuk janë në pronësi të TMS(E-turist), perveq përmbajtjeve nga TMS(E-turist)Studios. Secili kanal e ka pronarin e tij lehtësisht të identifikueshëm dhe secila video ose përmbajtje e plasuar në atë kanal, është përgjegjësi e plotë dhe e vetme e pronarit të Kanalit. TMS(E-turist) do t’ju ndihmoj në lehtësimin e konkaktit me pronarin e kanalit, mirëpo në asnjë rast, TMS(E-turist) nuk mban përgjegjësi për ato përmbajtje. Ju do t’i drejtoheni pronari të kanalit, për pretendimet në lidhje me atë video ose përmbajtje. </h4>
			
		
		
		</div>
	</div>
</div>

<?php include('includes/footeri.php');?>

<?php include('includes/signup.php');?>			

<?php include('includes/signin.php');?>			

<?php include('includes/write-us.php');?>			

</body>
</html>