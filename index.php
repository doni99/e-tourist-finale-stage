<?php
session_start();
error_reporting(0);
include('includes/config.php');
?>
<!DOCTYPE HTML>
<html>
<head>
<title>e-Tourist</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />

<link href="css/style.css" rel='stylesheet' type='text/css' />

<link href='//fonts.googleapis.com/css?family=Open+Sans:400,700,600' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
<link href="css/font-awesome.css" rel="stylesheet">
<!-- jquery prej github , video tut , edhe bootstrepi-->


<script src="js/jquery-1.12.0.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</head>
<body>
<?php include('includes/headeri.php');?>
<div class="banner">
	<div class="container">
		<h1> Mire se vini</h1>
	</div>
</div>



<div class="container">
	<div class="rupes">
		<div class="col-md-4 rupes-left wow fadeInDown animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">
			<div class="rup-left">
				<a href="offers.html"><i class="fa fa-usd"></i></a>
			</div>
			<div class="rup-rgt">
				<h3>KURSENI PARA</h3>
				<h4><a href="offers.html">OFERTAT ME TE MIRA</a></h4>
				
			</div>
				<div class="clearfix"></div>
		</div>
		<div class="col-md-4 rupes-left wow fadeInDown animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">
			<div class="rup-left">
				<a href="offers.html"><i class="fa fa-h-square"></i></a>
			</div>
			<div class="rup-rgt">
				<h3>AKOMODIM I SIGURT</h3>
				<h4><a href="offers.html">TEK HOTELET ME TE MIRA</a></h4>
				
			</div>
				<div class="clearfix"></div>
		</div>
		<div class="col-md-4 rupes-left wow fadeInDown animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">
			<div class="rup-left">
				<a href="offers.html"><i class="fa fa-mobile"></i></a>
			</div>
			<div class="rup-rgt">
				<h3>REZERVIME TE SHPEJTA</h3>
				<h4><a href="offers.html">EDHE PERMES TELEFONIT</a></h4>
			
			</div>
				<div class="clearfix"></div>
		</div>
	
	</div>
</div>
<!--- /rupes ---->


<!--- MIRE SE VINI , PAK ME SPJEGU QETU QKA T MUNEM  ---->


<!---<div class="col-md-4 rupes-left wow fadeInDown animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">
	<div class="rup-left">
	<h3>Mire se vini tek webi yne</h3>
	<img src="admin/pacakgeimages/<?php //echo htmlentities($result->PackageImage);?>" class="img-responsive" alt="">
	<h3>E-turist apo sistemi per menaxhimin e turizmit eshte nje platforme qe ndihmon</h3>
	<h3>ne gjetjen e ofertave me te mira te marrura prej agjensioneve me te mira ku , </h3>
	<h3>ne kete rast perdoruesi kursen kohe dhe sidomos para.</h3>
	<h3>E-turist apo sistemi per menaxhimin e turizmit eshte nje platforme qe ndihmon</h3>
	<h3>ne gjetjen e ofertave me te mira te marrura prej agjensioneve me te mira ku , </h3>
	<img src="admin/pacakgeimages/<?php //echo htmlentities($result->PackageImage);?>" class="img-responsive" alt="">
	<h3>ne kete rast perdoruesi kursen kohe dhe sidomos para.</h3>
	</div>
	</div>
	<div class="rup-left">
				<a href="offers.html"><i class="fa fa-mobile"></i></a>
			</div> ----> 


<!---ktu ofertat vendosen ---->
<div class="container">
	<div class="holiday">
	



	
	<h3>Lista e ofertave</h3>

					
<!---kqyrim e ndrrojm pak kodin ma vone ---->
<?php $sql = "SELECT * from tbltourpackages order by rand() limit 4";
$query = $dbh->prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($query->rowCount() > 0)
{
foreach($results as $result)
{	?>

	<!---90% E kryme , shtojm edhe diqka masanej , e bojm ma t mir ---->
			<div class="rom-btm">
				<div class="col-md-3 room-left wow fadeInLeft animated" data-wow-delay=".5s">
					<img src="admin/pacakgeimages/<?php echo htmlentities($result->PackageImage);?>" class="img-responsive" alt="">
				</div>
				<div class="col-md-6 room-midle wow fadeInUp animated" data-wow-delay=".5s">
					<h4>Emri: <?php echo htmlentities($result->PackageName);?></h4>
					<h6>Lloji : <?php echo htmlentities($result->PackageType);?></h6>
					<p><b>Lokacioni :</b> <?php echo htmlentities($result->PackageLocation);?></p>
					<p><b>Sherbimet</b> <?php echo htmlentities($result->PackageFetures);?></p>
				</div>

				<!---qetu mos e prekni se mdul probleme e keni ni tutorial se sdisha me ---->
				<div class="col-md-3 room-right wow fadeInRight animated" data-wow-delay=".5s">
					<h5><?php echo htmlentities($result->PackagePrice);?> EUR </h5>
					<a href="offert-detaje.php?pkgid=<?php echo htmlentities($result->PackageId);?>" class="view">Detajet</a>
				</div>
				<div class="clearfix"></div>
			</div>

<?php }} ?>
			
		
<div><a href="offert-list.php" class="view">Shiko me shume oferta</a></div>
</div>
			<div class="clearfix"></div>
	</div>



<!--- routes ---->
<div class="routes">
	<div class="container">
		<div class="col-md-4 routes-left wow fadeInRight animated" data-wow-delay=".5s">
			<div class="rou-left">
				<a href="#"><i class="glyphicon glyphicon-list-alt"></i></a>
			</div>
			<div class="rou-rgt wow fadeInDown animated" data-wow-delay=".5s">
				<h3>50+</h3>
				<p>Oferta</p>
			</div>
				<div class="clearfix"></div>
		</div>
		<div class="col-md-4 routes-left">
			<div class="rou-left">
				<a href="#"><i class="fa fa-user"></i></a>
			</div>
			<div class="rou-rgt">
				<h3>500+</h3>
				<p>Perdorues aktiv</p>
			</div>
				<div class="clearfix"></div>
		</div>
		<div class="col-md-4 routes-left wow fadeInRight animated" data-wow-delay=".5s">
			<div class="rou-left">
				<a href="#"><i class="fa fa-ticket"></i></a>
			</div>
			<div class="rou-rgt">
				<h3>4,000+</h3>
				<p>Rezervime</p>
			</div>
				<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<!--- footer , e pjest tjera, mos prekni qeto plz --->

<?php include('includes/footeri.php');?>

<?php include('includes/signup.php');?>		

<?php include('includes/signin.php');?>		

<?php include('includes/write-us.php');?>		

</body>
</html>